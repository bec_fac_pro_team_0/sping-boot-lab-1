# README #

Spring boot rest students api testing branching startegy.

### How to initiate a local git repo and sync with the remote bitbucket repo ###

* git init ---Initialize the git repository
* git remote -v --- Verify if a remote bitbucket url is linked with the local git rep
* git remote add origin <bitbucket-url> --- links local git repo with bitbucket repo
* git remote -v --- This should show newly linked bitbucket url
* git pull origin master --- this will bring the changes from bitbucket repo to local git repo
* git status --- check the status of files within the local repo
* git add . --- add all the modified files to set git to track them
* git commit -m "commit message" --- all the changed files are wrapped in to git version
* git push origin master --- push your local changes to remote repo
* git push --set-upstream origin master  --- push your local changes to remote repo and map local master to remote master.
