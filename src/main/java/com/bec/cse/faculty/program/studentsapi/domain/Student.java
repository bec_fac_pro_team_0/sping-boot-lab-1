package com.bec.cse.faculty.program.studentsapi.domain;

import org.springframework.context.annotation.ComponentScan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "STUDENT")
public class Student {
    @Id
    private String studentId;
    @Column
    private String firstName;
    @Column
    private String lastName;

    public Student() {

    }

//    public Student(String studentId, String firstName, String lastName) {
//        this.studentId = studentId;
//        this.firstName = firstName;
//        this.lastName = lastName;
//    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
