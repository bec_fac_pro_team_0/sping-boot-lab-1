package com.bec.cse.faculty.program.studentsapi.controller;

import com.bec.cse.faculty.program.studentsapi.domain.Student;
import com.bec.cse.faculty.program.studentsapi.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/students")
public class StudentsController {

    @Autowired
    StudentRepository studentRepository;

    @GetMapping
    public List<Student> getStudentsList() {
        try {
            List<Student> students = studentRepository.findAll();
            return students;
        } catch (Exception e) {
            return null;
        }
    }


    @GetMapping(value = "/{id}")
    public Student getStudent(@PathVariable String id) {
        Student student = studentRepository.findById(id).get();
        return student;
    }

    @PostMapping
    public String saveStudent(@RequestBody Student student) {
        studentRepository.save(student);
        return "Successfully saved student info";
    }

    @PutMapping
    public String editStudent(@RequestBody Student student) {
        studentRepository.save(student);
        return "Successfully edited student info";
    }

    @DeleteMapping(value = "/{id}")
    public void deleteStudent(@PathVariable String id) {
        studentRepository.delete(getStudent(id));
    }

    public List<Integer> filterEvenNumbers(List<Integer> list) {
        List<Integer> oddList = new ArrayList<>();
        for(int i = 0; i < list.size(); i ++) {
            if(list.get(i) % 2 != 0) {
                oddList.add(list.get(i));
            }
        }
        return oddList;
    }
}
