package com.bec.cse.faculty.program.studentsapi.controller;

import com.bec.cse.faculty.program.studentsapi.domain.Student;
import com.bec.cse.faculty.program.studentsapi.repository.StudentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StudentsControllerTest {

    @Mock
    StudentRepository repository;

    @InjectMocks
    StudentsController studentsController = new StudentsController();

    @Test
    public void testGetStudentsList() {
        List<Student> students = Arrays.asList(new Student());
        when(repository.findAll()).thenReturn(students);

        List<Student> studentList = studentsController.getStudentsList();

        assert studentList != null;
        assert studentList.size() == 1;
        verify(repository, times(1)).findAll();

    }

    @Test
    public void testSaveStudent() {
        Student student = new Student();
        when(repository.findAll()).thenThrow(new RuntimeException());

        String result = studentsController.saveStudent(student);

        assert result.equalsIgnoreCase("Successfully saved student info");

    }

    @Test
    public void testfilterEven() {
        List<Integer> students = Arrays.asList(1,2,3,4,5,6);

        List<Integer> list = studentsController.filterEvenNumbers(students);

        assert list.size() == 3;
        assert list.get(0) == 1;
        assert list.get(1) == 3;
        assert list.get(2) == 5;

    }
}
